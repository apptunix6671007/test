const jwt = require('jsonwebtoken');
const secretKey = 'Arya@2001';

function jwtAuth(req, res, next) {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ msg: 'No token provided' });
    }
 
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            console.log(err);
            return res.status(401).json({ msg: 'Failed to authenticate token' });
        }
        
        next();
    });
}

module.exports = jwtAuth;
