const express = require('express');
const authRoutes = require('./routes/authRoutes');
const postRoutes = require('./routes/postRoutes');
const uploadRoutes = require('./routes/uploadRoutes');
const commentRoutes=require('./routes/commentRoutes');
const mongoose=require("mongoose");
mongoose.connect('mongodb://localhost:27017/task');

const app = express();
app.use(express.json());
 
app.use('/auth',authRoutes);
app.use('/upload',uploadRoutes);
app.use('/post',postRoutes);
app.use('/comment',commentRoutes);

const PORT = process.env.PORT || 3333;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
