const jwt = require('jsonwebtoken');
const secretKey = 'Arya@2001';

const dummyUser = {
    id: 1,
    username: 'dummyuser',
    email: 'dummyuser@example.com',
};

function generateToken(user) {
    return jwt.sign(user, secretKey, { expiresIn: '2h' });
}

function login(req, res) {
    const token = generateToken(dummyUser);
    res.json({ token });
}

module.exports = { login };
