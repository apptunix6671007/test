const multer = require('multer');

// Set up Multer storage configuration
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/'); // Define destination folder for file uploads
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname); // Define file naming convention
    }
});

 
const upload = multer({ storage: storage });

 
function uploadFile(req, res) {
    if (!req.file) {
        return res.status(400).json({ msg: 'No file uploaded' });
    }
 
    res.status(200).json({ msg: 'File uploaded successfully', filename: req.file.filename });
}

module.exports = { uploadFile };
