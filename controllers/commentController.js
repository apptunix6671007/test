const Post = require('../models/Post');

 

async function addComment(req, res) {
    const { id, comment } = req.body; 
     
    try {
        const post = await Post.findOne({ _id: id });
        
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }
 
        post.comments.push({ user: id, comment });
        await post.save();
        res.status(201).json({ msg: 'Comment added successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal server error' });
    }
}
// Delete a comment from a post
async function deleteComment(req, res) {
    const { postId, commentId } = req.params;
    const user = req.user; 

    try {
         
        const post = await Post.findById(postId);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }

        
        const comment = post.comments.find(it => it._id.toString() === commentId);
        if (!comment) {
            return res.status(404).json({ msg: 'Comment not found' });
        }

        // Check if the user is authorized to delete the comment
        if (comment.user.toString() !== user.id) {
            return res.status(403).json({ msg: 'Unauthorized to delete this comment' });
        }

        // Remove the comment from the post
        post.comments = post.comments.filter(c => c._id.toString() !== commentId);
        await post.save();
        res.json({ msg: 'Comment deleted successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal server error' });
    }
}

module.exports = { addComment, deleteComment };
