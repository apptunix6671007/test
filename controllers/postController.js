const Post = require('../models/Post');

 
// Create post controller function
async function createPost(req, res) {
    const { user, content } = req.body;
    try {
        const newPost = await Post.create({ user, content, comments: [] });
        res.status(201).json({ msg: 'Post created successfully', post: newPost });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

// Update post controller function
async function updatePost(req, res) {
    const { postId } = req.params;
    const { content } = req.body;
    const userId = req.user.id; // Extract user ID from JWT token

    // Find post by ID and user ID
    const post = await Post.findOne({ _id: postId, user: userId });
    if (!post) {
        return res.status(404).json({ msg: 'Post not found or you are not authorized to update it' });
    }

    // Update post content
    post.content = content;
    await post.save();

    res.json({ msg: 'Post updated successfully', post });
}

async function viewLikes(req, res) {
    const { postId } = req.params;

    try {
        // Aggregate to get all likes associated with the post
        const likes = await Post.aggregate([
            { $match: { _id: postId } }, // Match the post by ID
            { $unwind: '$likes' }, // Deconstruct the likes array
            { $group: { _id: '$likes', count: { $sum: 1 } } } // Group by likes and count occurrences
        ]);

        res.json({ likes });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

async function likePost(req, res) {
    const { postId } = req.params;
    const userId = req.user.id;  

    try {
        // Find the post by ID
        const post = await Post.findById(postId);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }

        // Check if the user already liked the post
        if (post.likes.includes(userId)) {
            return res.status(400).json({ msg: 'You have already liked this post' });
        }

        // Add user ID to the likes array and save the post
        post.likes.push(userId);
        await post.save();

        res.json({ msg: 'Post liked successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

async function removeLike(req, res) {
    const { postId } = req.params;
    const userId = req.user.id; 

    try {
        
        const post = await Post.findById(postId);
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' });
        }
 
        if (!post.likes.includes(userId)) {
            return res.status(400).json({ msg: 'You have not liked this post' });
        }

        
        post.likes = post.likes.filter(like => like !== userId);
        await post.save();

        res.json({ msg: 'Like removed successfully' });
    } catch (error) {
        console.error(error);
        res.status(500).json({ msg: 'Internal Server Error' });
    }
}

module.exports = { createPost, updatePost, viewLikes, removeLike, likePost };
