const express = require('express');
const router = express.Router();
const jwtAuth = require('../middleware/jwtAuth');
const { addComment, deleteComment } = require('../controllers/commentController');

router.post('/comments', jwtAuth, addComment);
router.delete('/:postId/comments/:commentId', jwtAuth, deleteComment);

module.exports = router;
