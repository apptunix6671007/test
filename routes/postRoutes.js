const express = require('express');
const router = express.Router();
const jwtAuth = require('../middleware/jwtAuth');
const { createPost, updatePost, viewLikes,likePost ,removeLike} = require('../controllers/postController');

 
router.post('/create', jwtAuth, createPost);

 
router.put('/update/:postId', jwtAuth, updatePost);

 
router.get('/likes/:postId', jwtAuth, viewLikes);
router.post('/:postId/like', jwtAuth, likePost);
router.delete('/:postId/like', jwtAuth, removeLike);
module.exports = router;
